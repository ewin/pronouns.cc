# Terms of service

By using the pronouns.cc website and API (henceforth referred to as "the service"), you accept and agree to abide by the terms and provisions of this agreement.
If you do not agree to these Terms, do not use the service.

"We" refers to the developers and operators of pronouns.cc.

## General

The service is offered as is, without any warranty.

We reserve the right to terminate your account immediately, without prior notice or liability, for any reason whatsoever, including if you breach these Terms.

We reserve the right to modify these Terms and the Privacy Policy at any time. We will provide at least thirty days notice before any changes take effect.

## Accounts

You must be at least 13 years old and meet the minimum age required by the laws in your country to use the service.
If you are under 16 years old, you need consent from your parents or legal guardian to use the service.

## Content

You are responsible for the content you post on the service.
By posting it, you grant us a worldwide, royalty-free license to use, copy, reproduce, publish, modify (for example, resizing images before storing them), transmit, display, and distribute your content.

We do not endorse or guarantee the truthfulness of any content posted by other users of the service. We are not responsible for any harm, physical, mental or otherwise, resulting from content on the service.

You may not post any content that breaks the law or otherwise violates social norms, including, but not limited to:

- Support of totalitarian regimes
- Hate speech
- Racism and/or xenophobia
- Homophobia and/or transphobia
- Queerphobia
- Queer and/or plural exclusionism
- Sexism and/or misogyny
- Ableism
- Child pornography
- Pedophilia advocacy
- Harassment
- Impersonation
- Sharing someone’s personal information (doxxing)
- Encouraging self-harm and/or suicide
- Spam
- Trolling
- Advertisement
- Copyright and/or trademark violations

We reserve the right to remove content if we believe it violates these rules.
Additionally, we may report content that breaks the law to appropriate authorities.
